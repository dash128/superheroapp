import axios from 'axios'

const axiosInstance = axios.create({

    baseURL: "https://www.superheroapi.com/api.php/2580422828703823",
    headers: {
        'Content-Type': 'application/json',
    }

})

export default axiosInstance