import axiosInstance from './IAxios'
import {IHero} from '../interfaces/IHero'


const getHerosByName = async (name: string): Promise<Array<IHero>> => {

    let { data } = await axiosInstance.get("search/" + name)
    let dataJson: IResultGetHerosByName = data
    console.log(data)

    if (dataJson.response == "success") {
        return dataJson.results
    }

    return []
}
    


interface IResultGetHerosByName {
    response: string
    'results-for': string
    results: IHero[]
}

export {
    getHerosByName
}