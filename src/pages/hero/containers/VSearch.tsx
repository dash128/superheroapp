import React, { Component, Fragment } from 'react'
import SearchHeader from '../components/SearchHeader'
import ListHeros from '../components/ListHeros'
import {IHero} from '../../../interfaces/IHero'

class SearchView extends Component<IProps, IState> {

    constructor(props: IProps){
        super(props)
        this.state = {
            heros: [{
                "id": 485,
                "name": "Naruto Uzumaki",
                "powerstats": {
                    "intelligence": 50,
                    "strength": 80,
                    "speed": 32,
                    "durability": 80,
                    "power": 100,
                    "combat": 100
                },
                "biography": {
                    "full-name": "Naruto Uzumaki",
                    "alter-egos": "No alter egos found.",
                    "aliases": [
                        "-"
                    ],
                    "place-of-birth": "Konoha",
                    "first-appearance": "-",
                    "publisher": "Shueisha",
                    "alignment": "good"
                },
                "appearance": {
                    "gender": "Male",
                    "race": "Human",
                    "height": [
                        "5'6",
                        "168 cm"
                    ],
                    "weight": [
                        "121 lb",
                        "54 kg"
                    ],
                    "eye-color": "-",
                    "hair-color": "-"
                },
                "image": {
                    "url": "https://www.superherodb.com/pictures2/portraits/10/100/1540.jpg"
                }
            }]
        }
    }

    setHeros = (heros: IHero[]) => {this.setState({heros})}

    render() {

        return (
            <Fragment>
                <SearchHeader
                    updateHero={this.setHeros}/>
                <ListHeros heros={this.state.heros}/>
            </Fragment>
        )
    }
}

interface IProps{
    favorites: IHero[]
}

interface IState{
    heros: IHero[],
}

export default SearchView