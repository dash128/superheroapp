import React, { Fragment } from 'react'
import { makeStyles } from '@material-ui/core/styles';

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton'
import FavoriteIcon from '@material-ui/icons/Favorite'

import {IHero} from '../../../interfaces/IHero'

const useStyles = makeStyles({
    card: {
        maxWidth: 345,
    },
    media: {
        height: 240,
    },
})

const Hero = (props:IProps) => {
    const classes = useStyles()

    return (
        <Fragment>
            <Card className={classes.card}>
                <CardActionArea>
                    <CardMedia
                        className={classes.media}
                        image={props.hero.image.url}
                        title="Contemplative Reptile"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {props.hero.name}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            Hero of the {props.hero.appearance.race} race and {props.hero.appearance.gender} gender hero. 
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {props.hero.biography["full-name"]} born in {props.hero.biography["place-of-birth"]}.
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p"> 
                            Has a height of {props.hero.appearance.height[1]} and weight of {props.hero.appearance.weight[1]}.
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions disableSpacing>
                    <IconButton aria-label="add to favorites">
                        <FavoriteIcon />
                    </IconButton>
                </CardActions>
            </Card>
        </Fragment>
    )
}

interface IProps{
    hero: IHero
}

export default Hero