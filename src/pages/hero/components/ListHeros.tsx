import React, { Fragment } from 'react'
import Grid from '@material-ui/core/Grid'

import {IHero} from '../../../interfaces/IHero'

import Hero from './Hero'

const ListHeros = (props: IProps) => {

    return (
        <Fragment>
            <Grid container spacing={3}>
                {
                    props.heros.map( (hero:IHero) => (
                        <Grid item xs={6} sm={3} key={hero.id}>
                            <Hero hero={hero}/>
                        </Grid>
                    ) )
                }
            </Grid>
        </Fragment>
    )
}

interface IProps{
    heros: IHero[]
}

export default ListHeros