import React, { Fragment, useState } from 'react'

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { IHero } from '../../../interfaces/IHero';

import {getHerosByName} from '../../../axios/HeroService'

const SearchHeader = (props: IProps) => {

    const [search, setSearch] = useState("")

    const SearchHeroByName = async () => {
        let heros = await getHerosByName(search)

        if(heros.length==0){
            alert("character with given name not found")
        }

        props.updateHero(heros)

    }


    return (
        <Fragment>
            <TextField 
                id="outlined-basic"
                label="Outlined"
                variant="outlined"
                value={search} 
                onChange={e=> setSearch(e.target.value)} />
            <Button 
                variant="contained" 
                color="primary"
                onClick={() => SearchHeroByName()}>
                Buscar
            </Button>
        </Fragment>
    )
}

interface IProps{
    updateHero: (heros: IHero[]) =>void

}

export default SearchHeader