import React from 'react'
import { Link } from 'react-router-dom'

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const useStyle = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
        },
    })
)
const MyToolBar = () => {
    const classes = useStyle()

    return (
        <AppBar position='static' className={classes.root}>
            <Toolbar>
                <Typography variant="h6" className={classes.title}>Hero Apps 🐱‍🏍🦸🏼‍♂️</Typography>

                <Button color="inherit" component={Link} to="/">Home</Button>

                <Button color="inherit" component={Link} to='/myheros'>My Heros</Button>
            </Toolbar>
        </AppBar>
    )
}

export default MyToolBar