import React, { Component, Fragment } from 'react'
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom'

import CssBaseline from '@material-ui/core/CssBaseline'
import MyToolBar from './ToolBar'

import SearchView from '../hero/containers/VSearch'
import MyHerosView from '../hero/containers/VMyHeros'

import {IHero} from '../../interfaces/IHero'

class App extends Component<IProps, IState> {

    constructor(props: IProps){
        super(props)
        this.state = {
            favorites: []
        }
    }

    render() {
        return (
            <Fragment>
                <CssBaseline />
                <Router>
                    <MyToolBar />

                    <Switch>
                        <Route path="/myheros">
                            <MyHerosView favorites={this.state.favorites} />
                        </Route>
                        <Route path="/">
                            <SearchView favorites={this.state.favorites} />
                        </Route>
                    </Switch>

                </Router>
            </Fragment>

        )
    }
}

interface IProps{
   
}

interface IState{
    favorites: IHero[]
}

export default App