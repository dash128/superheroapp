export interface IHero {
    id: number;
    name: string;
    powerstats: Powerstats;
    biography: Biography;
    appearance: Appearance;
    image: {
        url: string;
    };
    
}

interface Powerstats {
    intelligence: number,
    strength: number,
    speed: number,
    durability: number,
    power: number,
    combat: number

}
interface Biography {
    'full-name': string,
    'alter-egos': string,
    aliases: Array<string>,
    'place-of-birth': string,
    'first-appearance': string,
    publisher: string,
    alignment: string
}

interface Appearance {
    gender: string,
    race: string,
    height: Array<string>,
    weight: Array<string>,
    'eye-color': string,
    'hair-color': string
}