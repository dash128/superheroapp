import React from 'react'
import ReactDOM from 'react-dom'

import App from '../pages/layout/App'

const app = document.getElementById('app')

ReactDOM.render(<App />, app)