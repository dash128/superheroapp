const path = require('path')
const HTMLWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    // entry: {
    //     "index": path.resolve(__dirname, './src/entries/index.tsx'), //'./src/index.tsx',
    // },
    entry: './src/entries/index.tsx',
    output: {
        path: path.resolve(__dirname, 'build/'),
        publicPath: '/',
        filename: 'bundle.js'
    },
    devServer: {
        port: 9009
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx", ".json"]
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: {
                    loader: 'awesome-typescript-loader'
                }
                // loader: "awesome-typescript-loader"
            },
            // {
            //     enforce: "pre",
            //     test: /\.js$/,
            //     loader: "source-map-loader"
            // },
            // {
            //     test: /\.scss$/,
            //     use: [
            //         MiniCssExtractPlugin.loader,
            //         "css-loader",
            //         "sass-loader"
            //     ]
            // }
        ]
    },
    plugins: [
        new HTMLWebpackPlugin({
            template: './src/entries/index.html'
        })
    ],
    devtool: 'eval-source-map',
}